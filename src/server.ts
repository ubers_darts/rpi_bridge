import SerialPort from 'serialport';
// @ts-ignore
import Delimiter from '@serialport/parser-delimiter';
import io from 'socket.io-client';
import { delay, InputTimeout } from './util';
import chalk from 'chalk';

process.on('unhandledRejection', up => { throw up });
chalk.level = 2;


const port_string =
  process.platform === "win32" ? 'COM4' :
    process.platform === "linux" ? '/dev/ttyACM0' :
      null;

const port = new SerialPort(port_string, {
  baudRate: 9600,
  dataBits: 8,
  stopBits: 1,
  parity: 'none',
});

port.on('error', function(err: { message: string }) {
  console.error('Error: ', err.message);
  delay(2000).then(() => port.open());
});

const parser = port.pipe(new Delimiter({ delimiter: '\n' }));


parser.on('data', (line: string) => {
  const str = line.toString();
  console.log('>', str);
});


const socket = io('http://www.starklab.ru:3001/bridge');

socket.on('connect', () => {
  console.log('connected to server');
});

socket.on('connect_timeout', (data: any) => {
  console.log('connection timeout', data.type);
});

socket.on('connect_error', (data: any) => {
  console.log('connection error', data.type);
});


socket.on('disconnect', (data: any) => {
  console.log('disconnected from server', data);
});





socket.on('fire', async (data: FireCommand) => {
  console.log('firing', data);
  port.write(`move ${data.x} ${data.y}\n`);
  await delay(1500);
  port.write(`valve ${data.duration}\n`);
  await delay(100);
  port.write('move 0 0\n');
});


interface FireCommand {
  x: number;
  y: number;
  duration: number;
}

import { EventEmitter } from "events";
import { setTimeout } from "timers";


export function delay(d: number) {
  return new Promise((res, rej) => {
    setTimeout(() => res(), d);
  })
}


export class InputTimeout extends EventEmitter {
  constructor(private timeout = 60000) {
    super()
  };
  private timeoutID?: NodeJS.Timeout;
  trigger() {
    if (this.timeoutID) {
      clearTimeout(this.timeoutID);
    }
    this.timeoutID = setTimeout(() => {
      clearTimeout(this.timeoutID);
      delete this.timeoutID;
      this.emit('complete');
    }, this.timeout);
  }
  get running() {
    return this.timeoutID !== undefined;
  }
}


export class RepeatTimeout {
  constructor() { };
  private timeoutID?: NodeJS.Timeout;
  private t_old: any;

  trigger(t: any, timeout = 60000, run: (t: any) => void) {
    if (this.t_old !== t) {
      this.t_old = t;
      this.reset();
    }
    if (!this.timeoutID) {
      run(t);
      this.timeoutID = setTimeout(() => {
        this.reset();
        this.trigger(t, timeout, run);
      }, timeout);
    }
  }


  reset() {
    if (this.timeoutID) {
      clearTimeout(this.timeoutID);
    }
    delete this.timeoutID;
  }
}
